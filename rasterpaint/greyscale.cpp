#include "greyscale.h"

/**
 * Constructor of the grayscale thread.
 * @param   parent    reference to a GreyScale class
 * @param   image     reference to on processed image
 */
GreyScaleThread::GreyScaleThread(GreyScale *parent, QImage& image) : image(image), parentObj(parent)
{
    connect(this,SIGNAL(finished()),parentObj,SLOT(inverted()));
}

/**
  * Main function of greyscale thread, change all colors to the matching grayscale color.
  */
void GreyScaleThread::run()
{
    for (int x=0; x<image.width();++x)
    {
        for (int y=0;y<image.height();++y)
        {
            QColor color(image.pixel(x,y));
            float d = color.red()*0.299 + color.green()*0.587 + color.blue()*0.114;
            color.setRed(d);
            color.setGreen(d);
            color.setBlue(d);
            image.setPixel(x,y,color.rgb());
        }
    }
}

/**
  * Function returning image in grayscale
  * @return image     processed image
  */
QImage GreyScaleThread::getImage()
{
    return image;
}

/**
 * Constructor of the grayscale class.
 * @param   parent    reference to the MainWindow class
 * @param   image     processed image
 */
GreyScale::GreyScale(QImage image, MainWindow *parent) :
    QObject(parent),
    FilterBase(parent),
    gsThread(this, image)
{
}

/**
  * Function starting a grayscale thread
  */
void GreyScale::start()
{
    gsThread.start();
}

/**
  * Function updating the processed image
  */
void GreyScale::inverted()
{
    mainPtr->updateImage(gsThread.getImage());
    deleteLater();
}
