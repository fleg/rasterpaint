#include <QApplication>
#include "settingsmanager.h"
#include "mainwindow.h"

/**
 * Main window of the application
 */

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();
    
    return a.exec();
}
