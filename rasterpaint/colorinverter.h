#ifndef COLORINVERTER_H
#define COLORINVERTER_H

#include "filterbase.h"
#include <QThread>
#include <QObject>

class ColorInverter;
/**
 * @brief Class dealing with inverter thread.
 */
class InverterThread : public QThread
{
public:
    InverterThread(ColorInverter* parent, QImage& image);
    void run();
    QImage getImage();

private:
    QImage image;
    ColorInverter* parentObj;
};
/**
 * @brief Class inverting colors of the processed image into opposite ones.
 */
class ColorInverter : public QObject, public FilterBase
{
    Q_OBJECT

public:
    ColorInverter(QImage image, MainWindow *parent);
    void start();

private:
    InverterThread invThread;

public slots:
    void inverted();
};

#endif // COLORINVERTER_H
