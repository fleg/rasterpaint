#ifndef NEWIMAGE_H
#define NEWIMAGE_H

#include "filterbase.h"
#include <QThread>
#include <QObject>



class NewImage;
/**
 * @brief Class dealing with new image thread.
 */
    class NewImageThread : public QThread
    {
    public:
        NewImageThread(NewImage* parent, QImage& image);
        void run();
        QImage getImage();

    private:
        QImage image;
        NewImage* parentObj;
    };
    /**
     * @brief Class clearing image.
     */
    class NewImage : public QObject, public FilterBase
    {
        Q_OBJECT

    public:
        NewImage(QImage image, MainWindow *parent);
        void start();

    private:
        NewImageThread niThread;

    public slots:
        void inverted();
    };

#endif // NEWIMAGE_H
