#include "fillsettings.h"
#include "ui_fillsettings.h"
#include "settingsmanager.h"
#include <QColorDialog>

/**
 * Constructor of the fill settings class.
 * @param   parent    reference to the widget with main window
 */
FillSettings::FillSettings(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::FillSettings)
{
    ui->setupUi(this);
}

/**
  * Destructor of the fill setings
  */
FillSettings::~FillSettings()
{
    delete ui;
}

/**
  * Function setting color of filling from Color Picker Dialog
  */
void FillSettings::on_selectFillColor_clicked()
{
    SettingsManager* i = SettingsManager::i();
    i->getBrush().setColor(QColorDialog::getColor(i->getBrush().color(),this));
}

/**
  * Function setting style of filling
  * @param  index   index of choosen fill style
  * @arg    1   - no fill
  * @arg    2   - half fill
  * @arg    3   - cross fill
  * @arg    4   - diagonal cross fill
  * @arg    0   - solid fill
  */
void FillSettings::on_comboBox_activated(int index)
{
    switch (index)
    {
    //none
    case 1:
        SettingsManager::i()->getBrush().setStyle(Qt::NoBrush);
        break;
    // half
    case 2:
        SettingsManager::i()->getBrush().setStyle(Qt::Dense4Pattern);
        break;
    // cross
    case 3:
        SettingsManager::i()->getBrush().setStyle(Qt::CrossPattern);
        break;
    // side
    case 4:
        SettingsManager::i()->getBrush().setStyle(Qt::DiagCrossPattern);
        break;
    // solid
    case 0:
    default:
        SettingsManager::i()->getBrush().setStyle(Qt::SolidPattern);
    }
}
