#include "textdialog.h"
#include "ui_textdialog.h"
#include <QFontDialog>
#include "settingsmanager.h"

/**
  * Constructor of the text dialog
  * @param  parent   reference to the Paint Widget
  * @param  pos      position of the text
  */
TextDialog::TextDialog(PaintWidget *parent, QPointF pos) :
    QDialog(parent),
    ui(new Ui::TextDialog),
    prnt(parent),
    pos(pos)
{
    ui->setupUi(this);
}

/**
  * Destructor of the text dialog
  */
TextDialog::~TextDialog()
{
    delete ui;
}

/**
  * Function dislaying fonts settings dialog
  */
void TextDialog::on_pushButton_clicked()
{
    QFontDialog dialog;
    bool fontSelected = false;
    dialog.setCurrentFont(SettingsManager::i()->getFont());
    QFont newFont = dialog.getFont(&fontSelected);
    if (fontSelected)
    {
        SettingsManager::i()->setFont(newFont);
    }
}

/**
  * Function inserting text to the processed image
  */
void TextDialog::on_buttonBox_accepted()
{
    SettingsManager::i()->setText(ui->lineEdit->text());
    prnt->addTextEvent(pos);
}
