#include "newimage.h"

/**
 * Constructor of the new image thread.
 * @param   parent    reference to a NewImage class
 * @param   image     reference to on processed image
 */
NewImageThread::NewImageThread(NewImage *parent, QImage& image) : image(image), parentObj(parent)
{
    connect(this,SIGNAL(finished()),parentObj,SLOT(inverted()));
}

/**
  * Main function of new image thread, clears the processed image.
  */
void NewImageThread::run()
{
    for (int x=0; x<image.width();++x)
    {
        for (int y=0;y<image.height();++y)
        {
            QColor color(image.pixel(x,y));
            color.setRed(255);
            color.setGreen(255);
            color.setBlue(255);
            image.setPixel(x,y,color.rgb());
        }
    }
}

/**
  * Function returning cleared image
  * @return image     processed image
  */
QImage NewImageThread::getImage()
{
    return image;
}

/**
 * Constructor of the new image class.
 * @param   parent    reference to the MainWindow class
 * @param   image     processed image
 */
NewImage::NewImage(QImage image, MainWindow *parent) :
    QObject(parent),
    FilterBase(parent),
    niThread(this, image)
{
}

/**
  * Function starting a new image thread
  */
void NewImage::start()
{
    niThread.start();
}

/**
  * Function updating the processed image
  */
void NewImage::inverted()
{
    mainPtr->updateImage(niThread.getImage());
    deleteLater();
}

