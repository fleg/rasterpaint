#ifndef GREYSCALE_H
#define GREYSCALE_H

#include "filterbase.h"
#include <QThread>
#include <QObject>



class GreyScale;
/**
 * @brief Class dealing with grayscale thread.
 */
    class GreyScaleThread : public QThread
    {
    public:
        GreyScaleThread(GreyScale* parent, QImage& image);
        void run();
        QImage getImage();

    private:
        QImage image;
        GreyScale* parentObj;
    };
    /**
     * @brief Class converting colors of the processed image into grayscale.
     */
    class GreyScale : public QObject, public FilterBase
    {
        Q_OBJECT

    public:
        GreyScale(QImage image, MainWindow *parent);
        void start();

    private:
        GreyScaleThread gsThread;

    public slots:
        void inverted();
    };


#endif // GREYSCALE_H
