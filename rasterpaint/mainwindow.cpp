#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFileDialog>
#include "imagetransformer.h"
#include "colorinverter.h"
#include "settingsmanager.h"
#include "pensettings.h"
#include "fillsettings.h"
#include "greyscale.h"
#include "newimage.h"

/**
 * Implementation of main window of user interface.
 */

/**
 * Constructor of the main window.
 * @param   parent    reference to the widget with main window
 */
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    paintWidget(new PaintWidget),
    penSettings(0)
{
    ui->setupUi(this);
    on_actionPencil_triggered();
    ui->scrollArea->setWidget(paintWidget);
    ui->scrollArea->setBackgroundRole(QPalette::Dark);
}

/**
  * Destructor of the main window
  */
MainWindow::~MainWindow()
{
    delete ui;
}

/**
  * Function dealing with opening images, displays Open File dialog, gets the name of the file and gives it to Paint Widget.
  * Deals also with size of the opened image.
  */
void MainWindow::openImageAction()
{
    QString fileName = QFileDialog::getOpenFileName(this);
    paintWidget->openImage(fileName);
    paintWidget->adjustSize();
}

/**
  * Function dealing with saving images.
  */
void MainWindow::saveImageAction()
{
    //TODO: Make "Save" behave differently that "Save As"
    saveAsImageAction();
}

/**
  * Function dealing with saving images, displays Save File dialog, gets the name of the file and gives it to Paint Widget.
  */
void MainWindow::saveAsImageAction()
{
    QString fileName = QFileDialog::getSaveFileName(this);
    paintWidget->saveImage(fileName);
}

/**
  * Function dealing with clearing images. Invoke NewImage class.
  */
void MainWindow::newImageAction()
{
    NewImage* ni = new NewImage(paintWidget->getImage(), this);
    ni->start();
}

/**
  * Function dealing with displaying resize/rotate dialog. Invoke ImageTransformer class.
  */
void MainWindow::resizeRotateAction()
{
    ImageTransformer* transformer = new ImageTransformer(paintWidget->getImage(),this);
    transformer->show();
}

/**
  * Function dealing with inverting images. Invoke ColorInverter class.
  */
void MainWindow::invertColorsAction()
{
    ColorInverter* inverter = new ColorInverter(paintWidget->getImage(), this);
    inverter->start();
}

/**
  * Function dealing with transforming images into grey scale. Invoke GreyScale class.
  */
void MainWindow::greyScaleAction()
{
    GreyScale* gs = new GreyScale(paintWidget->getImage(), this);
    gs->start();
}

/**
  * Will update the image in PaintWidget with image given in parameter
  * @param  image   processed image
  */
void MainWindow::updateImage(QImage image)
{
    paintWidget->setImage(image);
}

/**
  * Function dealing with checking the pencil icon, unchecking another icons and invoking function drawPoints.
  */
void MainWindow::on_actionPencil_triggered()
{
    ui->actionLine->setChecked(false);
    ui->actionRect->setChecked(false);
    ui->actionText->setChecked(false);
    ui->actionPencil->setChecked(true);
    ui->actionRubber->setChecked(false);
    SettingsManager* sm = SettingsManager::i();
    sm->setActivity(drawPoints);
    sm->setRubber(false);
}

/**
  * Function dealing with checking the text icon, unchecking another icons and invoking function drawText.
  */
void MainWindow::on_actionText_triggered()
{
    ui->actionLine->setChecked(false);
    ui->actionRect->setChecked(false);
    ui->actionText->setChecked(true);
    ui->actionPencil->setChecked(false);
    ui->actionRubber->setChecked(false);
    SettingsManager* sm = SettingsManager::i();
    sm->setActivity(drawText);
}

/**
  * Function dealing with checking the line icon, unchecking another icons and invoking function drawLines.
  */
void MainWindow::on_actionLine_triggered()
{
    ui->actionLine->setChecked(true);
    ui->actionRect->setChecked(false);
    ui->actionText->setChecked(false);
    ui->actionPencil->setChecked(false);
    ui->actionRubber->setChecked(false);
    SettingsManager* sm = SettingsManager::i();
    sm->setActivity(drawLines);
}

/**
  * Function dealing with checking the rectangles icon, unchecking another icons and invoking function drawRects.
  */
void MainWindow::on_actionRect_triggered()
{
    ui->actionLine->setChecked(false);
    ui->actionRect->setChecked(true);
    ui->actionText->setChecked(false);
    ui->actionPencil->setChecked(false);
    ui->actionRubber->setChecked(false);
    SettingsManager* sm = SettingsManager::i();
    sm->setActivity(drawRects);
}

/**
  * Function dealing with checking the rubber icon, unchecking another icons and invoking function drawPoints.
  */
void MainWindow::on_actionRubber_triggered()
{
    ui->actionLine->setChecked(false);
    ui->actionRect->setChecked(false);
    ui->actionText->setChecked(false);
    ui->actionPencil->setChecked(false);
    ui->actionRubber->setChecked(true);
    SettingsManager* sm = SettingsManager::i();
    sm->setActivity(drawPoints);
    sm->setRubber(true);
}

/**
  * Function displaying and hiding the Pen Setting Dialog.
  */
void MainWindow::on_actionPenColor_triggered(bool pushed)
{
    if (pushed)
    {
        // display the pen settings
        Q_ASSERT(0 == penSettings);
        penSettings = new QDockWidget(tr("Pen"),this);
        PenSettings* ps = new PenSettings(penSettings);
        ps->setWindowFlags(Qt::Widget);
        penSettings->setWidget(ps);
        addDockWidget(Qt::RightDockWidgetArea, penSettings);
    }
    else
    {
        // hide the pen settings
        Q_ASSERT(0 != penSettings);
        removeDockWidget(penSettings);
        penSettings->deleteLater();
        penSettings = 0;
    }
}

/**
  * Function displaying and hiding the Fill Setting Dialog.
  */
void MainWindow::on_actionFillColor_triggered(bool checked)
{
    if (checked)
    {
        // display the fill settings
        Q_ASSERT(0 == fillSettings);
        fillSettings = new QDockWidget(tr("Fill"),this);
        FillSettings* ps = new FillSettings(fillSettings);
        fillSettings->setWidget(ps);
        addDockWidget(Qt::RightDockWidgetArea, fillSettings);
    }
    else
    {
        // hide the fill settings
        Q_ASSERT(0 != fillSettings);
        removeDockWidget(fillSettings);
        fillSettings->deleteLater();
        fillSettings = 0;
    }
}
