#ifndef TEXTDIALOG_H
#define TEXTDIALOG_H

#include <QDialog>
#include "paintwidget.h"



namespace Ui {
class TextDialog;
}
/**
 * @brief Class dealing with text settings dialog.
 */
class TextDialog : public QDialog
{
    Q_OBJECT
    
public:
    TextDialog(PaintWidget *parent, QPointF);
    ~TextDialog();
    
private slots:
    void on_pushButton_clicked();

    void on_buttonBox_accepted();

private:
    Ui::TextDialog *ui;
    PaintWidget* prnt;
    QPointF pos;
};

#endif // TEXTDIALOG_H
