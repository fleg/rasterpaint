#ifndef PENSETTINGS_H
#define PENSETTINGS_H

#include <QDialog>



namespace Ui {
class PenSettings;
}
/**
 * @brief Class dealing with pen settings dialog.
 */
class PenSettings : public QDialog
{
    Q_OBJECT

public:
    explicit PenSettings(QWidget *parent);
    ~PenSettings();
    
private slots:
    void on_spinBox_valueChanged(int arg1);

    void on_colorSelectBtn_clicked();

    void on_comboBox_activated(int index);

private:
    Ui::PenSettings *ui;
    static PenSettings* instance;
};

#endif // PENSETTINGS_H
