#ifndef IIMAGEFILTER_H
#define IIMAGEFILTER_H

#include <QImage>
#include "mainwindow.h"

/**
 * @brief Image Filter base class
 *
 * How should image filters work:
 * \li Image filter class object is created with its own copy
 * of the image and pointer to the main window
 * \li Filter class should do its job (may or may not display
 * any window messages and whatnot)
 * \li If the image is changed after the filter is applied,
 * public slot MainWindow::updateImage(QImage) should be called
 * with the new image in the parameter.
 * If image was not changed (for example user clicked the cancel
 * button), nothing should be done in this step.
 * \li Class should destroy itself autonomously using QObjects
 * using the deleteLater() slot.
 */
class FilterBase
{
public:
    FilterBase(MainWindow *parent);

protected:
    MainWindow* mainPtr;
};

#endif // IIMAGEFILTER_H
