#include "paintwidget.h"
#include <QPainter>
#include "settingsmanager.h"
#include "textdialog.h"

/**
 * Constructor of the paint widget class.
 * @param   parent    reference to the widget with main window
 */
PaintWidget::PaintWidget(QWidget *parent) :
    QWidget(parent),
    image(500,500,QImage::Format_RGB32),
    isDrawing(false)
{
    setAttribute(Qt::WA_StaticContents);

    image.fill(qRgb(255,255,255));
}

/**
  * Function drawing image on the screen
  */
void PaintWidget::paintImage()
{
    QPainter painter(this);
    painter.drawImage(0,0,image);
    setMinimumSize(sizeHint());
}

/**
  * Function dealing with paint event
  */
void PaintWidget::paintEvent(QPaintEvent *)
{
    paintImage();
}

/**
  * Function dealing with opening images
  * @returns if image was opened properly
  */
bool PaintWidget::openImage(QString filename)
{
    if (image.load(filename))
    {
        paintImage();

        update();
        updateGeometry();
        return true;
    }
    return false;
}

/**
  * Function dealing with saving images
  * @returns if image was saved properly
  */
bool PaintWidget::saveImage(QString filename)
{
    return image.save(filename);
}

/**
  * Function returning size of the processed image
  * @return image size
  */
QSize PaintWidget::sizeHint() const
{
    return image.size();
}

/**
  * Function returning the processed image
  * @return image
  */
QImage PaintWidget::getImage() const
{
    return image;
}

/**
  * Function setting the processed image
  */
void PaintWidget::setImage(QImage image)
{
    this->image = image;
    this->repaint();
}

/**
  * Function dealing with mouse move event
  * @param  ev  mouse event
  */
void PaintWidget::mouseMoveEvent(QMouseEvent *ev)
{
    if (isDrawing)
    {

        switch(SettingsManager::i()->getActivity())
        {
        case drawPoints:
        {
            QPainter painter(&image);
            currentPath.lineTo(ev->posF());
            painter.strokePath(currentPath,SettingsManager::i()->getPen());
            painter.end();
            break;
        }
        default:
            break;
        }
        repaint();
    }
}

/**
  * Function dealing with mouse press event
  * @param  ev  mouse event
  */
void PaintWidget::mousePressEvent(QMouseEvent *ev)
{
    currentPath = QPainterPath();
    currentPath.moveTo(ev->posF());
    initialPoint = ev->posF();
    previousImage = image.copy();
    isDrawing = true;
}

/**
  * Function dealing with add text event
  * @param  pos  text position
  */
void PaintWidget::addTextEvent(QPointF pos)
{
    QPainter painter(&image);
    painter.setPen(SettingsManager::i()->getPen());
    painter.setFont(SettingsManager::i()->getFont());
    painter.drawText(pos,SettingsManager::i()->getText());
}

/**
  * Function dealing with mouse release event
  * @param  ev  mouse event
  */
void PaintWidget::mouseReleaseEvent(QMouseEvent *ev)
{
    isDrawing = false;
    switch(SettingsManager::i()->getActivity())
    {
    case drawLines:
    {
        QPainter painter(&image);
        currentPath.lineTo(ev->posF());
        painter.strokePath(currentPath,SettingsManager::i()->getPen());
        painter.end();
        break;
    }
    case drawRects:
    {
        QPainter painter(&image);
        painter.setPen(SettingsManager::i()->getPen());
        painter.setBrush(SettingsManager::i()->getBrush());
        QRectF rect(initialPoint,ev->posF());
        currentPath.addRect(rect);
        painter.drawPath(currentPath);
        painter.end();
        break;
    }
    case drawText:
    {
        TextDialog* textDialog = new TextDialog(this,ev->posF());
        textDialog->show();
        break;
    }
    default:
        break;
    }
    repaint();
}
