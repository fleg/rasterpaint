#ifndef IMAGETRANSFORMER_H
#define IMAGETRANSFORMER_H

#include <QDialog>
#include <QImage>
#include "mainwindow.h"
#include "filterbase.h"

namespace Ui {
class ImageTransformer;
}

/**
 * @brief Class resizing and rotating images
 *
 * Objects of this class delete themselves after they are done with their job.
 */
class ImageTransformer : public QDialog, public FilterBase
{
    Q_OBJECT
    
public:
    explicit ImageTransformer(QImage image, MainWindow *parent);
    ~ImageTransformer();
    
private:
    Ui::ImageTransformer *ui;
    QImage image;

private slots:
    void accept();
    void reject();
};

#endif // IMAGETRANSFORMER_H
