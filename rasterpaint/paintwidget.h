#ifndef PAINTWIDGET_H
#define PAINTWIDGET_H

#include <QWidget>
#include <QImage>
#include <QMouseEvent>
#include <QPainterPath>

/**
 * @brief Class dealing with opening and saving images and with mouse events.
 */
class PaintWidget : public QWidget
{
    Q_OBJECT
public:
    explicit PaintWidget(QWidget *parent = 0);
    bool openImage(QString filename);
    bool saveImage(QString filename);
    QSize sizeHint() const;
    QImage getImage() const;
    void setImage(QImage image);

private:
    QImage image, previousImage;
    QPointF initialPoint;
    void paintImage();
    void paintEvent(QPaintEvent *);
    bool isDrawing;
    QPainterPath currentPath;
    
signals:
    
public slots:
    void mousePressEvent(QMouseEvent *);
    void mouseMoveEvent(QMouseEvent *);
    void mouseReleaseEvent(QMouseEvent *);
    void addTextEvent(QPointF);
    
};

#endif // PAINTWIDGET_H
