#include "colorinverter.h"

/**
 * Constructor of the inverter thread.
 * @param   parent    reference to a ColorConverter class
 * @param   image     reference to on processed image
 */
InverterThread::InverterThread(ColorInverter *parent, QImage& image) : image(image), parentObj(parent)
{
    connect(this,SIGNAL(finished()),parentObj,SLOT(inverted()));
}

/**
  * Main function of inverter thread, change all colors to the opposite ones.
  */
void InverterThread::run()
{
    for (int x=0; x<image.width();++x)
    {
        for (int y=0;y<image.height();++y)
        {
            QColor color(image.pixel(x,y));
            color.setRed((color.red() - 255)*-1);
            color.setGreen((color.green() - 255)*-1);
            color.setBlue((color.blue() - 255)*-1);
            image.setPixel(x,y,color.rgb());
        }
    }
}

/**
  * Function returning inverted image
  * @return image     processed image
  */
QImage InverterThread::getImage()
{
    return image;
}

/**
 * Constructor of the color inverter class.
 * @param   parent    reference to the MainWindow class
 * @param   image     processed image
 */
ColorInverter::ColorInverter(QImage image, MainWindow *parent) :
    QObject(parent),
    FilterBase(parent),
    invThread(this, image)
{
}

/**
  * Function starting an inverter thread
  */
void ColorInverter::start()
{
    invThread.start();
}

/**
  * Function updating the processed image
  */
void ColorInverter::inverted()
{
    mainPtr->updateImage(invThread.getImage());
    deleteLater();
}

