#include "pensettings.h"
#include "ui_pensettings.h"
#include "settingsmanager.h"
#include <QColorDialog>

/**
 * Constructor of the pen settings class.
 * @param   parent    reference to the widget with main window
 */
PenSettings::PenSettings(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::PenSettings)
{
    ui->setupUi(this);
}

/**
  * Destructor of the pen setings
  */
PenSettings::~PenSettings()
{
    delete ui;
}

/**
  * Function setting thicness of the pencil and border
  */
void PenSettings::on_spinBox_valueChanged(int arg1)
{
    SettingsManager::i()->setPenFatness(arg1);
}

/**
  * Function displaying color picker dialog
  */
void PenSettings::on_colorSelectBtn_clicked()
{
    SettingsManager* i = SettingsManager::i();
    i->getPen().setColor(QColorDialog::getColor(i->getPen().color(),this));
}

/**
  * Function setting shape of pencil
  */
void PenSettings::on_comboBox_activated(int index)
{
    switch (index)
    {
    case 1:
        SettingsManager::i()->getPen().setCapStyle(Qt::RoundCap);
        break;
    case 0:
    default:
        SettingsManager::i()->getPen().setCapStyle(Qt::SquareCap);
    }
}
