#-------------------------------------------------
#
# Project created by QtCreator 2012-11-16T20:47:12
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = rasterpaint
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    paintwidget.cpp \
    imagetransformer.cpp \
    filterbase.cpp \
    colorinverter.cpp \
    settingsmanager.cpp \
    textdialog.cpp \
    pensettings.cpp \
    fillsettings.cpp \
    greyscale.cpp \
    newimage.cpp

HEADERS  += mainwindow.h \
    paintwidget.h \
    imagetransformer.h \
    filterbase.h \
    colorinverter.h \
    settingsmanager.h \
    textdialog.h \
    pensettings.h \
    fillsettings.h \
    greyscale.h \
    newimage.h

FORMS    += mainwindow.ui \
    imagetransformer.ui \
    textdialog.ui \
    pensettings.ui \
    fillsettings.ui

RESOURCES += \
    icons.qrc
