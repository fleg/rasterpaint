#include "settingsmanager.h"

SettingsManager* SettingsManager::ptr=0;

/**
  * Constructor of Setting Manager class
  */
SettingsManager::SettingsManager() :
    activity(drawPoints),
    brush(Qt::SolidPattern),
    rubberPen(Qt::white)
{
}

SettingsManager* SettingsManager::i()
{
    if (!ptr) ptr = new SettingsManager();
    return ptr;
}

/**
  * Function checking if actualy using tool is pencil or rubber
  */
QPen &SettingsManager::getPen()
{
    if(isRubber())
        return rubberPen;
    else
        return pen;
    Q_ASSERT("WHOAH THIS SHOULDNT HAPPEN LIKE, NEVER" == 0);
    return pen;
}

/**
  * Function setting thickness of pencil, rubber and border
  * @param fatness  thickness of pencil, rubber and border
  */
void SettingsManager::setPenFatness(int fatness)
{
    pen.setWidth(fatness);
    rubberPen.setWidth(fatness);
}

/**
  * Function setting that actualy using tool is pencil
  */
void SettingsManager::setPen(QPen &pen)
{
    this->pen = pen;
}

/**
  * Function checking what activity had been choosen
  * @return activity
  */
Activity SettingsManager::getActivity()
{
    return activity;
}

/**
  * Function setting activity
  * @param  act  activity
  */
void SettingsManager::setActivity(Activity act)
{
    activity = act;
}

/**
  * Function getting inserted text
  * @return text
  */
QString &SettingsManager::getText()
{
    return text;
}

/**
  * Function setting inserted text
  * @param  text    inserted text
  */
void SettingsManager::setText(QString text)
{
    this->text = text;
}

/**
  * Function getting font of inserted text
  * @return font
  */
QFont &SettingsManager::getFont()
{
    return font;
}

/**
  * Function setting font of inserted text
  */
void SettingsManager::setFont(QFont font)
{
    this->font = font;
}

/**
  * Function getting brush
  * @return brush
  */
QBrush &SettingsManager::getBrush()
{
        return brush;
}

/**
  * Function setting brush
  * @param  brush
  */
void SettingsManager::setBrush(QBrush brush)
{
    this->brush = brush;
}

/**
  * Function checking if actualy useing tool is rubber
  * @return rubber
  */
bool SettingsManager::isRubber()
{
    return rubber;
}

/**
  * Function setting rubber
  * @param  rubber
  */
void SettingsManager::setRubber(bool rubber)
{
    this->rubber = rubber;
}
