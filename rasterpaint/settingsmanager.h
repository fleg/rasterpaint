#ifndef SETTINGSMANAGER_H
#define SETTINGSMANAGER_H
#include <QPen>
#include <QFont>

enum Activity
{
    drawPoints,
    drawLines,
    drawRects,
    drawText,
    drawCircle
};

/**
 * @brief Singleton holding all settings for the application
 */
class SettingsManager
{
public:
    /// Returns an instance of SettingsManager.
    static SettingsManager* i();
    QPen& getPen();
    void setPen(QPen&);
    Activity getActivity();
    void setActivity(Activity act);
    QString& getText();
    void setText(QString text);
    QFont& getFont();
    void setFont(QFont font);
    QBrush& getBrush();
    void setBrush(QBrush brush);
    bool isRubber();
    void setRubber(bool rubber);
    void setPenFatness(int fatness);
protected:
    SettingsManager();
    static SettingsManager* ptr;
    QPen pen;
    Activity activity;
    QString text;
    QFont font;
    QBrush brush;
    bool rubber;
    QPen rubberPen;
};

#endif // SETTINGSMANAGER_H
