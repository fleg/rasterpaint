#include "imagetransformer.h"
#include "ui_imagetransformer.h"

/**
 * Constructor of the image transformer.
 * @param   parent    reference to a MainWindow class
 * @param   image     reference to on processed image
 */
ImageTransformer::ImageTransformer(QImage image, MainWindow *parent) :
    QDialog(parent),
    FilterBase(parent),
    ui(new Ui::ImageTransformer),
    image(image)

{
    ui->setupUi(this);
}

/**
  * Destructor of the fill setings
  */
ImageTransformer::~ImageTransformer()
{
    delete ui;
}

/**
  * Function resising and rotating image. Hides resize/rotate dialog.
  */
void ImageTransformer::accept()
{
    this->hide();
    if (ui->resizeImageGroup->isChecked())
    {
        image = image.scaled(ui->resizeWidthBox->value(),
                             ui->resizeHeightBox->value());
    }
    if (ui->rotateImageGroup->isChecked())
    {
        QTransform transform;
        transform.rotate(ui->angleSlider->value());
        image = image.transformed(transform);
    }
    this->mainPtr->updateImage(image);
    this->deleteLater();
}

/**
  * Hides resize/rotate dialog.
  */
void ImageTransformer::reject()
{
    this->hide();
    this->deleteLater();
}
