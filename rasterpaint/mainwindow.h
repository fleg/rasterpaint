#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QDockWidget>
#include "paintwidget.h"



namespace Ui {
class MainWindow;
}
/**
 * @brief Main window of user interface
 */
class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    
private:
    Ui::MainWindow *ui;
    PaintWidget* paintWidget;
    QDockWidget* penSettings;
    QDockWidget* fillSettings;

public slots:
    void openImageAction();
    void saveImageAction();
    void newImageAction();
    void saveAsImageAction();
    void resizeRotateAction();
    void invertColorsAction();
    void greyScaleAction();
    void updateImage(QImage image);
private slots:
    void on_actionPencil_triggered();
    void on_actionText_triggered();
    void on_actionLine_triggered();
    void on_actionRect_triggered();
    void on_actionPenColor_triggered(bool pushed);
    void on_actionFillColor_triggered(bool checked);
    void on_actionRubber_triggered();
};

#endif // MAINWINDOW_H
