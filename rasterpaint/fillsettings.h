#ifndef FILLSETTINGS_H
#define FILLSETTINGS_H

#include <QWidget>



namespace Ui {
class FillSettings;
}
/**
 * @brief Class dealing with fill settings dialog.
 */
class FillSettings : public QWidget
{
    Q_OBJECT
    
public:
    explicit FillSettings(QWidget *parent = 0);
    ~FillSettings();
    
private slots:
    void on_selectFillColor_clicked();

    void on_comboBox_activated(int index);

private:
    Ui::FillSettings *ui;
};

#endif // FILLSETTINGS_H
